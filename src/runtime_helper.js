//global: _SECP256K1

//Provides: wasm_int_pointer
//Requires: caml_ba_from_typed_array
function wasm_int_pointer(x) {
  return caml_ba_from_typed_array(new joo_global_object.Uint32Array([x]));
}

//Provides: wasm_ints_pointer
//Requires: caml_ba_from_typed_array
function wasm_ints_pointer(x) {
  return caml_ba_from_typed_array(new joo_global_object.Uint32Array(x));
}

//Provides:call_wasm
//Requires: Ml_Bigarray, caml_failwith
function call_wasm() {
  var g = joo_global_object;
  var M = g._SECP256K1;
  var f = arguments[0];
  if (!M[f]) {
    caml_failwith(f + " is not implemented");
  }
  var args = Array.prototype.slice.call(arguments, 1);
  var argsc = args.slice();
  // argsu is used to handle the case where multiple arguments
  // are physically the same.
  // We don't want to copy the piece of memory multiple time into wasm
  var argsu = new g.Map();

  for (var i = 0; i < args.length; i++) {
    var x = args[i];
    if (typeof x == "number" || typeof x == "boolean") {
      continue;
    } else if (x instanceof Ml_Bigarray) {
      if (argsu.get(x)) {
        argsc[i] = argsu.get(x);
      } else {
        var p = M._malloc(x.data.length * x.data.BYTES_PER_ELEMENT);
        switch (x.data.BYTES_PER_ELEMENT) {
          case 1:
            M.HEAPU8.set(x.data, p);
            break;
          case 4:
            M.HEAPU32.set(x.data, p >> 2);
            break;
          default:
            caml_failwith(
              "call_wasm: unsuported buffer of elements of size " +
                x.data.BYTES_PER_ELEMENT
            );
        }
        argsu.set(x, p);
        argsc[i] = p;
      }
    } else {
      var r = "(";
      for (i = 0; i < args.length; i++) {
        if (i != 0) r += ", ";
        var err = "*ERR*";
        var x = args[i];
        var typeof_x = typeof x;
        // Valid argument type
        if (typeof_x == "number" || typeof_x == "boolean") r += typeof_x;
        else if (x instanceof Ml_Bigarray) r += "bigarray";
        // Invalid argument type
        else if (x == null) r += err + "null";
        else if (x == undefined) r += err + "undefined";
        else r += err + typeof_x;
      }
      r += ")";
      joo_global_object.console.error(
        "call_wasm: unsupported argument type: " + f + r,
        new Error()
      );
      caml_failwith(
        "call_wasm: " + f + " called with unsupported argument type: " + r
      );
    }
  }
  var r = M[f].apply(null, argsc);

  // Copy the memory back into the JS world
  var it = argsu.entries();
  var e;
  while (((e = it.next()), !e.done)) {
    var e = e.value;
    var x = e[0];
    var p = e[1];
    if (x instanceof Ml_Bigarray) {
      switch (x.data.BYTES_PER_ELEMENT) {
        case 1:
          x.data.set(new g.Uint8Array(M.HEAPU8.buffer, p, x.data.length));
          break;
        case 4:
          x.data.set(new g.Uint32Array(M.HEAPU32.buffer, p, x.data.length));
          break;
        default:
          caml_failwith("call_wasm: unsuported buffer");
      }
      M._free(p);
    }
  }
  return r;
}

//Provides: secp256k1_wasm_allocate
function secp256k1_wasm_allocate(x) {
  var M = joo_global_object._SECP256K1;
  var ptr = M._malloc(x.data.length);
  M.HEAPU8.set(x.data, ptr);
  return ptr;
}

//Provides: secp256k1_wasm_update
function secp256k1_wasm_update(x, ptr) {
  var M = joo_global_object._SECP256K1;
  var data = new joo_global_object.Uint8Array(
    M.HEAPU8.buffer,
    ptr,
    x.data.length
  );
  x.data.set(data);
  M._free(ptr);
  return 0;
}

//Provides: secp256k1_wasm_free
function secp256k1_wasm_free(ptr) {
  var _SECP256K1 = joo_global_object._SECP256K1;
  return _SECP256K1._free(ptr);
}
